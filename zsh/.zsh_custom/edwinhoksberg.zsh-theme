# Modified default 'tjkirch' theme

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$fg[red]%}⚡"
ZSH_THEME_GIT_PROMPT_CLEAN=""

ZSH_THEME_GIT_PROMPT_STASH_COUNT_BEFORE=" %{$fg[red]%} "
ZSH_THEME_GIT_PROMPT_STASH_COUNT_AFTER="%{$reset_color%}"

function git_stash_count() {
    local COUNT
    COUNT=$(command git stash list 2> /dev/null | wc -l) 

    [ "$COUNT" -gt 0 ] && echo "$ZSH_THEME_GIT_PROMPT_STASH_COUNT_BEFORE$COUNT$ZSH_THEME_GIT_PROMPT_STASH_COUNT_AFTER"
}

PROMPT='%(?, ,%{$fg[red]%}FAIL: $?%{$reset_color%}
)

%{$fg[magenta]%}%n%{$reset_color%}@%{$fg[yellow]%}%m%{$reset_color%}: %{$fg_bold[blue]%}%~%{$reset_color%}$(git_prompt_info)$(git_stash_count)
$ '

RPROMPT='%{$fg[green]%}[%*]%{$reset_color%}'
