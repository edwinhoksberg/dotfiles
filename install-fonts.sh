#!/bin/bash

set -xe

cd /tmp

font_dir=~/.local/share/fonts/
mkdir -p $font_dir

# Install font-awesome fonts
sudo apt install fonts-font-awesome


# Install source code pro fonts
foldername="source-code-pro-2.030R-ro-1.050R-it"

wget -q https://github.com/adobe-fonts/source-code-pro/archive/2.030R-ro/1.050R-it.tar.gz -O $foldername.tar.gz
tar zxf $foldername.tar.gz --wildcards "${foldername}/TTF/*.ttf"

rm $foldername.tar.gz
mv $foldername/TTF/*.ttf $font_dir


# Install ubuntu mono powerline font
files=( "https://github.com/powerline/fonts/raw/master/UbuntuMono/Ubuntu%20Mono%20derivative%20Powerline%20Bold%20Italic.ttf" "https://github.com/powerline/fonts/raw/master/UbuntuMono/Ubuntu%20Mono%20derivative%20Powerline%20Bold.ttf" "https://github.com/powerline/fonts/raw/master/UbuntuMono/Ubuntu%20Mono%20derivative%20Powerline%20Italic.ttf" "https://github.com/powerline/fonts/raw/master/UbuntuMono/Ubuntu%20Mono%20derivative%20Powerline.ttf" )

for f in "${files[@]}"; do
    wget -q $f -P $font_dir
done


# Update font cache
fc-cache -rv
